CC = cc
CFLAGS = -lXm -lXt -lX11 -Wall -ansi -pedantic

all: dtrun

dtrun:	dtrun.c
	$(CC) $(CFLAGS) -o $@ dtrun.c
	strip dtrun

clean:
	rm dtrun
