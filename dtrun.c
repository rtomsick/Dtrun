/*
 * Copyright (C) 2012 Robert Tomsick <robert@tomsick.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * dtrun.c
 *
 * Provides a simple runner application designed to be invoked via a hotkey or
 * keystroke combination (ala. other operating systems' "Run" dialog).
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include <Xm/Text.h>

#define PROG_NAME "Dtrun"
#define IS_WHITESPACE(c) \
    (c == ' ' || c == '\t' || \
        c == '\v' || c == '\r' || \
        c == '\n')

/* button callbacks */
static void cb_run(Widget, XtPointer, XmPushButtonCallbackStruct*);
static void cb_cancel(Widget, XtPointer, XmPushButtonCallbackStruct*);
/* keypress callback for bail on esc */
static void cb_esc(Widget, XtPointer, XEvent*);

/**
 * Fork and execute using specified arguments.
 */
static void do_exec(char**);

/**
 * Split command string into arguments.
 */
static char**
split_cmd_str(const char*);


int
main(int argc, char* argv[])
{
    XtAppContext    app;
    Widget          toplevel, form, command, btnRun, btnCancel;

    XmString        run_label, cancel_label;

    Arg             args[1];

    toplevel = XtVaOpenApplication(&app, PROG_NAME, NULL, 0, &argc, argv,
                                    NULL, sessionShellWidgetClass,
                                    NULL);


    run_label = XmStringCreateLocalized("Run");
    cancel_label = XmStringCreateLocalized("Cancel");

    form = XtVaCreateManagedWidget("form", xmFormWidgetClass, toplevel,
                                    XmNnoResize, 1,
                                    NULL);


    command = XmVaCreateText(form, "command_text",
                            XmNwidth, 200,
                            /* pos */
                            XmNtopAttachment, XmATTACH_FORM,
                            XmNtopOffset, 5,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset, 5,
                            XmNleftAttachment, XmATTACH_FORM,
                            XmNleftOffset, 5,
                            NULL);

    /* start command input off with focus */
    XtSetArg(args[0], XmNinitialFocus, command);
    XtSetValues(form, args, 1);

    /* run button */
    btnRun = XmVaCreatePushButton(form, "run_button",
                                    /* button text */
                                    XmNlabelType, XmSTRING,
                                    XmNlabelString, run_label,
                                    /* pos */
                                    XmNtopAttachment, XmATTACH_FORM,
                                    XmNtopOffset, 7,
                                    XmNbottomAttachment, XmATTACH_FORM,
                                    XmNbottomOffset, 5,
                                    XmNleftAttachment, XmATTACH_WIDGET,
                                    XmNleftWidget, command,
                                    XmNleftOffset, 10,
                                    NULL);
    XtAddCallback(btnRun, XmNactivateCallback,
                    (XtCallbackProc)cb_run, (XtPointer)command);
    /* give the command widget the same callback when it's activated */
    XtAddCallback(command, XmNactivateCallback,
                    (XtCallbackProc)cb_run, (XtPointer)command);
    XmStringFree(run_label);

    /* cancel button */
    btnCancel = XmVaCreatePushButton(form, "cancel_button",
                                        /* button text */
                                        XmNlabelType, XmSTRING,
                                        XmNlabelString, cancel_label,
                                        /* positioning */
                                        XmNtopAttachment, XmATTACH_FORM,
                                        XmNtopOffset, 7,
                                        XmNbottomAttachment, XmATTACH_FORM,
                                        XmNbottomOffset, 5,
                                        XmNleftAttachment, XmATTACH_WIDGET,
                                        XmNleftWidget, btnRun,
                                        XmNleftOffset, 10,
                                        XmNrightAttachment, XmATTACH_FORM,
                                        XmNrightOffset, 5,
                                        /* not elligible for focus */
                                        XmNtraversalOn, 0,
                                        NULL);
    XtAddCallback(btnCancel, XmNactivateCallback,
                    (XtCallbackProc)(*cb_cancel), NULL);
    XmStringFree(cancel_label);

    /* finally, attach a handler to bail on esc */
    XtAddEventHandler(toplevel, KeyPressMask, 0, (XtEventHandler) cb_esc,
                        NULL);
    XtGrabKey(toplevel, XKeysymToKeycode(XtDisplay(toplevel), XK_Escape), 0,
                1, GrabModeAsync, GrabModeAsync);

    XtManageChild(form);
    XtManageChild(command);
    XtManageChild(btnRun);
    XtManageChild(btnCancel);

    XtRealizeWidget(toplevel);

    XtAppMainLoop(app);

    return EXIT_SUCCESS;
}

static void
cb_run(Widget w, XtPointer cd, XmPushButtonCallbackStruct* cbs)
{
    /* before we try to run the command string, check to make sure it's
     * not empty and not just whitespace
     */
    char* command_str = XmTextGetString((Widget)cd);
    char* r = command_str;

    do
    {
        if (! IS_WHITESPACE(*r))
        {
            break;
        }
    } while (*r++ != '\0');

    if (*r == '\0')
    {
        return;
    }

    do_exec(split_cmd_str(command_str));
}

static void
cb_cancel(Widget w, XtPointer client_data,
        XmPushButtonCallbackStruct* cbs)
{
    exit(EXIT_SUCCESS);
}

static void
cb_esc(Widget w, XtPointer cd, XEvent* ev)
{
    if (ev->xkey.keycode == XKeysymToKeycode(XtDisplay(w), XK_Escape))
    {
        exit(EXIT_SUCCESS);
    }
}

static void
do_exec(char** args)
{
    pid_t pid;

    pid = fork();

    if (pid == 0)
    {
        execvp(args[0], args);
    }
    else
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }
    else
    {
        perror("fork() failed");
    }

    /* either failed exec or failed fork */
    exit(EXIT_FAILURE);
}

static char**
split_cmd_str(const char* str)
{
    char** args = malloc(strlen(str) * sizeof (char*));
    char** ret = args;
    char* buf = malloc(strlen(str));

    strcpy(buf, str);

    while (*buf != '\0')
    {
        /* we null out whitespace to both ignore extraneous spaces and to
         * terminate our arguments appropriately
         */
        while (IS_WHITESPACE(*buf))
        {
            *buf++ = '\0';
        }

        *args++ = buf;

        while (*buf != '\0' && ! IS_WHITESPACE(*buf))
        {
            buf++;
        }
    }

    *args = NULL;

    return ret;
}
